var winston = require("winston");
winston.info("Nexis module initialized, Winston logging active...");

var fs = require("fs");
var os = require('os'); 
os.tmpDir = os.tmpdir;
const path = require("path");

const { Builder, By, Key, until } = require("selenium-webdriver");
var webdriver = require("selenium-webdriver");
var extend = require('selenium-extend');
var chrome = require("selenium-webdriver/chrome");

//gcp setup
const Firestore = require("@google-cloud/firestore");
const storage = require('@google-cloud/storage');
const bucketName = "mindwise-83ddb.appspot.com";
const gcs = storage({
  projectId: 'mindwise-83ddb',
  keyFilename: '../config/mindwise-83ddb.json'
});
const admin = require("firebase-admin");

//locators
var locators = require("./nexisLocators.js");
var utility = require("../utility.js");

//initialize firestore
var keyName = require('../config/mindwise-firebase-admin.json');
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("profiles")
  .doc("nexis")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => joker(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });


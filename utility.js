var winston = require("winston");
winston.info("Utility module initialized, Winston logging active...");

var fs = require("fs");
var os = require('os'); 
os.tmpDir = os.tmpdir;
const path = require("path");

const {Builder, By, Key, until } = require("selenium-webdriver");
var webdriver = require("selenium-webdriver");
var extend = require('selenium-extend');
var chrome = require("selenium-webdriver/chrome");

var rn = require("random-number");
var easyimg = require("easyimage");
var anticaptcha = require("./captcha/anticaptcha.js");


var keyName = require("./config/mindwise-83ddb.json");

var GlobalTimeout = 50000;
exports.GlobalTimeout;


//I don't know why this works
var xext = -10;
var yext = -10;
var eleHeight = 0;
var eleWidth = 0;

const spath = "./image/image.jpg";
const dpath = "./image/cropped.jpg";

module.exports = {
  takeScreenshot: async function (driverVal, path) {
    try {
      var base64Image = await driverVal.takeScreenshot();
      var decodedImage = new Buffer(base64Image, "base64");
      fs.writeFileSync(path, decodedImage);
      winston.log("Web Page Screenshot Saved at:" + path);
    } catch (err) {
      winston.debug("Failure taking web page screenshot.");
      winston.error(err.message);
      throw err;
    }
  },

  base64_encode: async function (file) {
    var bitmap = fs.readFileSync(file);
    return new Buffer(bitmap).toString("base64");
  },

  /* crops particular WebElement from screenshot of whole page */
  getElementSnap: async function (driver, locator, spath, dpath) {
    try {
      var dimensions = await driver.findElement(By.xpath(locator)).getRect();
      var xLoc = xext + dimensions.x;
      var yLoc = yext + dimensions.y;
      var eWidth = eleWidth + (xLoc * 2 - 300);
      var eHeight = eleHeight + yLoc;

      await cropInFile(xLoc, yLoc, eWidth, eHeight, spath, dpath);
      winston.info("WebElement screenshot cropped, saved at: " + dpath);
    } catch (err) {
      winston.debug("Failed to get element snapshot.");
      winston.error(err.message);
      throw err;
    }
  },

  /* checks if a WebElement actually exists */
  verifyElement: async function (driver, locator, timeout) {
    try {
      await driver.wait(until.elementLocated(By.xpath(locator)), timeout);
      return true;
    } catch (err) {
      winston.debug("Could not find element : " + locator);
      return false;
    }
  },

  clickElement: async function (driver, locator, timeout) {
    //check element exists
    await this.verifyElement(driver, locator, timeout);

    //hover over element
    //var el = await driver.findElement(By.xpath(locator));
    //how to hover?

    //hesitate for a random period
    var options = {
      min: 0,
      max: 3000,
      integer: true
    };
    var delay = rn(options);
    await driver.sleep(delay);

    //click
    await driver.findElement(By.xpath(locator)).click();
  },

  enterText: async function (driver, locator, timeout, text) {
    //make sure element exists
    await this.verifyElement(driver, locator, timeout);

    //hover over element
    var options = {
      min: 0,
      max: 150,
      integer: true
    };
    var offset = rn(options);
    //hover
    //how to hover properly?

    //click element
    await driver.findElement(By.xpath(locator)).click();

    //clear field
    await driver.findElement(By.xpath(locator)).clear();

    try {
      //simulating psuedorandom pauses between successive keystrokes
      for (var i = 0; i < text.length; i++) {
        var options = {
          min: 0,
          max: 2000,
          integer: true
        };
        var delay = rn(options);
        await driver.sleep(delay);
        await driver.findElement(By.xpath(locator)).sendKeys(text[i]);
      }
    } catch (err) {
      winston.info("An error occured attempting to enter text on the element with locator: " + locator);
      throw err;
    }
  },

  /* selects an option value from dropdown on partial text*/
  selectByPartialText: async function (
    driverVal,
    locator,
    timeout,
    partialText
  ) {
    try {
      await this.verifyElement(driverVal, locator, timeout);
      await this.clickElement(driverVal, locator, timeout);
      var optionLocator = locator + "//option[contains(text(),'" + partialText + "')]";
      await driverVal.sleep(1000);
      await driverVal.findElement(By.xpath(optionLocator)).click();
    } catch (err) {
      throw err;
    }
  },

  /* selects an option value from dropdown on value */
  selectByValue: async function (driver, locator, timeout, value) {
    try {
      await this.verifyElement(driver, locator, timeout);
      await this.clickElement(driver, locator, timeout);
      var optionLocator = locator + "//option[@value='" + value + "']";
      await driver.sleep(500);
      await driver.findElement(By.xpath(optionLocator)).click();
    } catch (err) {
      throw err;
    }
  },

  /* Helpful functions for file manipulation*/
  createFile: async function (fileName, data) {
    fs.writeFile(fileName, data, function (err) {
      if (err) throw err;
    });
  },

  login: async function (driver, loginMap) {
    var captchaCounter = 0;
    do {
      //increment our captcha attempts captchaCounter
      captchaCounter = captchaCounter + 1;

      //navigate to url
      await driver.get(loginMap.get("url"));

      //look for auth elements
      await this.enterText(
        driver,
        loginMap.get("userNameEdit"),
        GlobalTimeout,
        loginMap.get("userNameVal")
      );
      await this.enterText(
        driver,
        loginMap.get("passwordEdit"),
        GlobalTimeout,
        loginMap.get("passwordVal")
      );

      //crop target element
      await this.takeScreenshot(driver, spath);
      await this.getElementSnap(
        driver,
        loginMap.get("captchaImage"),
        spath,
        dpath
      );

      //push captcha to firebase storage for analysis
      var captchaBucket = loginMap.get("name") + "-captcha";
      var captchaFileName = loginMap.get("name") + "_" + new Date().getTime().toString();

      var encoded = await this.base64_encode(dpath);

      //send to anticaptcha
      var code = await anticaptcha.solveCaptcha(encoded, "");
      var flag = true;

      //check for captcha errors
      if (
        code === "false" ||
        code.indexOf("error") > -1 ||
        code.indexOf("BLANK") > -1 ||
        code === "NONE" ||
        code.indexOf("ERROR") > -1
      ) {
        flag = false;
      } else {
        //captcha solved succesfully
        await driver.findElement(By.xpath(loginMap.get("captchaEdit"))).sendKeys(code);

        //login
        await driver.findElement(By.xpath(loginMap.get("loginButton"))).sendKeys(Key.ENTER);

        //check for confirmationElement indicating login was succesful
        flag = await this.verifyElement(
          driver,
          loginMap.get("confirmationElement"),
          7000
        );
      }
      if (flag) {
        break;
      } else {
        //captcha was incorrect
        winston.info(
          "Captcha Invalid -- Attempt Number: " + captchaCounter.toString()
        );
        await driver.sleep(2000);
      }
    } while (captchaCounter < loginMap.get("captchaAttempts"));

    //we are out of captcha attempts :(
    if (captchaCounter === loginMap.get("captchaAttempts")) {
      throw new Error(
        "Failed to solve captcha in " +
        loginMap.get("captchaAttempts") +
        " attempts"
      );
    }
  }
};

async function cropInFile(
  xext,
  yext,
  eleWidth,
  eleHeight,
  sourcePath,
  destinationPath
) {
  try {
    await easyimg.crop({
      src: sourcePath,
      dst: destinationPath,
      cropwidth: eleWidth,
      cropheight: eleHeight,
      x: xext,
      y: yext,
      gravity: "North-West"
    });
  } catch (err) {
    winston.info("Failed to crop " + sourcePath);
    throw err;
  }
}
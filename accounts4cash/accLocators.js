//login elements
exports.usernameEdit = "/html/body/div[1]/header/nav/div/ul";
exports.passwordEdit = "//input[@id='regPass']";
exports.capthaImage = "//div[img[contains(@src,'captcha')]]";
exports.captchaEdit = "//input[@id='keystring']";
exports.loginButton = "//button[@id='signin']";

//shopping selectors
exports.accountsLink = "/html/body/div[1]/aside/section/ul/li[1]/ul";
exports.itemCountSelect = "//select[@name='count1']";
exports.pageNavigation = "//a[@class='changeP'][@uid='replaceable']";
exports.firstRow = "//table[@id='items']//tbody//tr[1]";
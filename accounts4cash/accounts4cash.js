//Parameters To Be Received 
const captchaAttempts = 5;
var gcFolder = 'Acc4Cash';
var userName = 'lulzpid';
var password = 'Jlennon2';
var url = 'http://accs4cash.com';


const {Builder, By, Key, until} = require('selenium-webdriver');

var locators = require('./accLocators.js');
var utility = require('../utility.js');
var chrome = require("selenium-webdriver/chrome");
var webdriver = require("selenium-webdriver");
const admin = require('firebase-admin');




var outputPath = "C:\\dev\\martingale\\output\\";

async function accForCash() {

  var ts = await (new Date).getTime().toString();

  var driver = await new webdriver.Builder().forBrowser('chrome').build();

  try {
    var loginMap = await new Map();
    await loginMap.set('name', 'Acc4Cash');
    await loginMap.set('url', url);
    await loginMap.set('captchaAttempts', captchaAttempts);
    await loginMap.set('userNameEdit', locators.usernameEdit);
    await loginMap.set('passwordEdit', locators.passwordEdit);
    await loginMap.set('captchaImage', locators.capthaImage);
    await loginMap.set('captchaEdit', locators.captchaEdit);
    await loginMap.set('loginButton', locators.loginButton);
    await loginMap.set('confirmationElement', locators.pageNavigation);
    await loginMap.set('userNameVal', userName);
    await loginMap.set('passwordVal', password);

    await utility.login(driver, loginMap);

    await utility.clickElement(driver,locators.accountsLink,utility.GlobalTimeout);
    await driver.sleep(5000);
    await utility.selectByValue(driver, locators.itemCountSelect, utility.GlobalTimeout,'125');
    await driver.sleep(5000);

    var flag = true;
    var pageCounter = 1;
    do{

        flag = await utility.verifyElement(driver, locators.firstRow, utility.GlobalTimeout);
        if(flag){
          var fileName = await outputPath + 'Acc4Cash_Page_' + pageCounter + '_' + ts + '.html';
          await console.log(fileName);
          await driver.sleep(1000);


          //push html to disk
          var data = await driver.getPageSource();
          await fs.writeFile(fileName, data);

          //push html to google cloud
          await utility.uploadToGC(fileName, gcFolder);


          //Handle Next
          pageCounter = await pageCounter + 1;
          flag = await utility.verifyElement(driver,locators.pageNavigation.replace('replaceable', pageCounter), 5000);
          if(await flag){
            await utility.clickElement(driver, locators.pageNavigation.replace('replaceable', pageCounter), utility.GlobalTimeout);
            await driver.sleep(1000);
          }
        }
    }
    while(flag);

    await console.log('Successful execution for Accounts4Cash.');

  } finally {
    await driver.quit();
  }

}
accForCash();
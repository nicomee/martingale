//login elements
exports.usernameEdit = "//*[@id='username']";
exports.passwordEdit = "//*[@id='pass']";
exports.capthaImage = "/html/body/div[2]/div/div/div[2]/form/div[3]";
exports.captchaEdit = "//*[@id='captcha']";
exports.shopNavBar = "//*[@id='navbarShop']";
exports.topNav = "/html/body/nav";


//navbar elements
exports.nonrefundableShop = "/html/body/nav/div/div[1]/ul/li[1]/div/a[2]";
exports.refundableShop = "/html/body/nav/div/div[1]/ul/li[1]/div/a[1]";
exports.autobuyShop = "/html/body/nav/div/div[1]/ul/li[1]/div/a[3]";

//bank filters
exports.baseFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[1]/div/select";
exports.binFilter = "//*[@id='bin']";
exports.bankFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[3]/div[2]/div/div/select";

//location filters
exports.countryFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[5]/div[1]/div[1]/div/select";
exports.zipFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[5]/div[2]/div[1]/div/select";
exports.stateFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[5]/div[2]/div[1]/div/select";
exports.cityFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[5]/div[2]/div[1]/div/select";

//card filters
exports.cardtypeFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[7]/div[2]/div/div/select";
exports.cardlevelFilter = "/html/body/div[2]/div[1]/div/div/div[1]/div/div/form/div[5]/div[2]/div[1]/div/select";

//buttons
exports.selectBtn = "//*[@id='cui10800703']/div/div/button[1]";

//lists page elements
exports.binList = "/html/body/div[2]/div[1]/div/div/div[2]/div/div/form/div[1]/textarea";
exports.zipList = "/html/body/div[2]/div[1]/div/div/div[2]/div/div/form/div[2]/textarea";

var winston = require("winston");
winston.info("Bankomat module initialized, Winston logging active...");

const { Builder, By, Key, until } = require("selenium-webdriver");
const Firestore = require("@google-cloud/firestore");
const admin = require("firebase-admin");
const functions = require("firebase-functions");
var fs = require("fs");

var locators = require("./bankomatLocators.js");
var utility = require("../utility.js");
var chrome = require("selenium-webdriver/chrome");
var firefox = require("selenium-webdriver/firefox");

var webdriver = require("selenium-webdriver");

//initialize firestore
var keyName = require("../config/mindwise.json");
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("identities")
  .doc("jokerstash")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => joker(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });

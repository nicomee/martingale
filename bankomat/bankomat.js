var winston = require("winston");
winston.info("Winston logging active....");

const { Builder, By, Key, until } = require("selenium-webdriver");
const Firestore = require("@google-cloud/firestore");
const admin = require("firebase-admin");
const functions = require("firebase-functions");
var fs = require("fs");

var locators = require("./jokerLocators.js");
var utility = require("../utility.js");
var chrome = require("selenium-webdriver/chrome");
var firefox = require("selenium-webdriver/firefox");

var webdriver = require("selenium-webdriver");

//initialize firestore
var keyName = require("../config/mindwise-08dd54fcba5a.json");
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("identities")
  .doc("jokerstash")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => joker(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });

 const {
     Builder,
     By,
     Key,
     until
 } = require('selenium-webdriver');
 const Firestore = require('@google-cloud/firestore');
 const admin = require('firebase-admin');
 const functions = require('firebase-functions');
 var fs = require('fs');

 var locators = require('./bankomatLocators.js');
 var utility = require('../utility.js');
 var chrome = require("selenium-webdriver/chrome");
 var webdriver = require("selenium-webdriver");

 //initialize firestore
 admin.initializeApp({
     credential: admin.credential.cert("C:\\dev\\martingale\\auth\\mindwise-08dd54fcba5a.json")
 });
 var db = admin.firestore();



 //listen for new identities
 var identities = [];

 function quickstartListen(db) {
     db.collection('identities').get()
         .then((snapshot) => {
             snapshot.forEach((doc) => {
                 logger.info(doc.id, '=>', doc.data());
                 identities.push(doc.data());
             });
         })
         .catch((err) => {
             logger.error('Error getting identities: ', err);
         });
 }

 async function bankomat() {
     await logger.info(identities.toString());

     var driver = await new webdriver.Builder().forBrowser('chrome').build();
     var loginMap = await new Map();
     await loginMap.set('name', 'bankomat');
     await loginMap.set('url', url);
     await loginMap.set('captchaAttempts', captchaAttempts);
     await loginMap.set('userNameEdit', locators.usernameEdit);
     await loginMap.set('passwordEdit', locators.passwordEdit);
     await loginMap.set('captchaImage', locators.capthaImage);
     await loginMap.set('captchaEdit', locators.captchaEdit);
     await loginMap.set('loginButton', locators.loginButton);
     await loginMap.set('confirmationElement', locators.topNav);
     await loginMap.set('userNameVal', userName);
     await loginMap.set('passwordVal', password);

     await utility.login(driver, loginMap);


     //navigate to nonrefundable shop
     await driver.get('http://bankomat.cc/shop/');


     //wait until form loads
     //filter by base
     await utility.clickElement(driver, locators.baseFilter, 5000);
     await driver.findElement(By.css('div.btns:nth-child(2) > button:nth-child(2)')).click();


     //inject javascript due to the AJAX
     await driver.executeScript("var baseSelector = document.querySelector('input[type='text']');baseSelector.click();");
     await driver.sleep(10000);

 }

 quickstartListen(db);
 bankomat();
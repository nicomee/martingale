var winston = require("winston");
winston.info("JokerStash module initialized, Winston logging active...");

var fs = require("fs");
var os = require('os'); 
os.tmpDir = os.tmpdir;
const path = require("path");
const mime = require("mime-types");

const { Builder, By, Key, until } = require("selenium-webdriver");
var webdriver = require("selenium-webdriver");
var extend = require('selenium-extend');
var chrome = require("selenium-webdriver/chrome");

const Firestore = require("@google-cloud/firestore");
const storage = require('@google-cloud/storage');
const bucketName = "mindwise-83ddb.appspot.com";
const gcs = storage({
  projectId: 'mindwise-83ddb',
  keyFilename: '../config/mindwise-83ddb.json'
});
const admin = require("firebase-admin");



var locators = require("./jokerLocators.js");
var utility = require("../utility.js");

//initialize firestore
var keyName = require('../config/mindwise-firebase-admin.json');
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("profiles")
  .doc("jokerstash")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => baseFetch(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });


  async function baseFetch(stuff) {
    //load blockchain-dns extension
    options = new chrome.Options();
    options.addArguments("--load-extension=/extension");
    options.addArguments("--start-maximized");
  
    //intialize driver
    var driver = new webdriver.Builder()
      .setChromeOptions(options)
      .withCapabilities({
        browserName: "chrome",
        name: "Chrome",
        tz: "America/Los_Angeles",
        build: "Chrome Build",
        idleTimeout: "60"
      })
      .build();
      extend.addExtend(driver);
  
  
    //intialize local variables
    var baseids = stuff.baseids;
    var extract = stuff.extract;
  
    var gcFolder = 'jstash-' + extract;
    var outputPath = '../output/jokerstash/' + extract + '/';
  
  
    //populate loginMap
    var loginMap = new Map();
     loginMap.set("name", "jstash");
     loginMap.set("url", stuff.url);
     loginMap.set("captchaAttempts", stuff.captcha);
     loginMap.set("userNameEdit", locators.usernameEdit);
     loginMap.set("passwordEdit", locators.passwordEdit);
     loginMap.set("captchaImage", locators.captchaImage);
     loginMap.set("captchaEdit", locators.captchaEdit);
     loginMap.set("loginButton", locators.loginButton);
     loginMap.set("confirmationElement", locators.userNameHeader);
     loginMap.set("userNameVal", stuff.username);
     loginMap.set("passwordVal", stuff.pwd);
  
    //login
    await utility.login(driver, loginMap);


    //get cards bases
    var cardBases = [];
    await driver.get(stuff.url + 'cards');
    await driver.sleep(5000);
    await utility.verifyElement(driver, locators.baseFilter, 8000);

    driver.findElement(By.xpath(locators.baseFilter)).then(function(webElement) {
                    webElement.findElements(By.tagName('option')).then(function(optionArray){
                        if(optionArray.length > 1){
                                  optionArray[0].getAttribute('value').then(function(optionValue) {
                                      console.log(optionValue);
                                      cardBases.push(optionValue);
                 });
                        }
                    });
                 }, function(err) {
                });
    
  }
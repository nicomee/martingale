var winston = require("winston");
winston.info("JokerStash module initialized, Winston logging active...");

var fs = require("fs");
var os = require('os'); 
os.tmpDir = os.tmpdir;
const path = require("path");
const mime = require("mime-types");

const { Builder, By, Key, until } = require("selenium-webdriver");
var webdriver = require("selenium-webdriver");
var extend = require('selenium-extend');
var chrome = require("selenium-webdriver/chrome");

//gcp setup
const Firestore = require("@google-cloud/firestore");
const storage = require('@google-cloud/storage');
const bucketName = "mindwise-83ddb.appspot.com";
const gcs = storage({
  projectId: 'mindwise-83ddb',
  keyFilename: '../config/mindwise-83ddb.json'
});
const admin = require("firebase-admin");


//locators
var locators = require("./jokerLocators.js");
var utility = require("../utility.js");


//initialize firestore
var keyName = require('../config/mindwise-firebase-admin.json');
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("profiles")
  .doc("jokerstash")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => joker(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });

  //uploads a file from disk to firebase storage bucket
  async function uploadToGC(filePath, folderName) {
    var ts =  new Date().getTime().toString();
    var fileName = path.basename(filePath) + "_" + ts;

    const bucket =  gcs.bucket(bucketName);
    const uploadTo = folderName + "/" + fileName;
    const fileMime =  mime.lookup(filePath);

    try {
      await bucket.upload(filePath, {
        destination: uploadTo,
        public: false,
        metadata: {
          contentType: fileMime,
          cacheControl: "public, max-age=300"
        }
      });
    } catch (err) {
      throw err;
    }
  }
async function joker(stuff) {
  //load blockchain-dns extension
  options = new chrome.Options();
  options.addArguments("--load-extension=/extension");
  options.addArguments("--start-maximized");

  //intialize driver
  var driver = new webdriver.Builder()
    .setChromeOptions(options)
    .withCapabilities({
      browserName: "chrome",
      name: "Chrome",
      tz: "America/Los_Angeles",
      build: "Chrome Build",
      idleTimeout: "60"
    })
    .build();
    //add selenium-extend capabilities
    extend.addExtend(driver);


  //intialize local variables
  var baseids = stuff.baseids;
  var extract = stuff.extract;

  var gcFolder = 'jstash-' + extract;
  var outputPath = '../output/jokerstash/' + extract + '/';


  //populate loginMap
  var loginMap = new Map();
   loginMap.set("name", "jstash");
   loginMap.set("url", stuff.url);
   loginMap.set("captchaAttempts", stuff.captcha);
   loginMap.set("userNameEdit", locators.usernameEdit);
   loginMap.set("passwordEdit", locators.passwordEdit);
   loginMap.set("captchaImage", locators.captchaImage);
   loginMap.set("captchaEdit", locators.captchaEdit);
   loginMap.set("loginButton", locators.loginButton);
   loginMap.set("confirmationElement", locators.userNameHeader);
   loginMap.set("userNameVal", stuff.username);
   loginMap.set("passwordVal", stuff.pwd);

  //login
  await utility.login(driver, loginMap);

  //iterate through baseids
  for (var i = 0, len = baseids.length; i < len; i++) {
    var instance_url = stuff.url + extract + "?base=" + baseids[i];
    var cart_url = stuff.url + "cart";
    // Check if string or number is supplied
    var data = parseInt(baseids[i], 10);
    if (isNaN(data)) {
      //base is new, and does not yet have an id available for us to filter on
      await driver.get(stuff.url + extract);
      await driver.sleep(5000);

      //select the base & apply the filters
      await utility.selectByPartialText(
        driver,
        locators.baseFilter,
        utility.GlobalTimeout,
        baseids[i]
      );
      await utility.clickElement(driver, locators.applyFilters, 4000);

    } else {
     

       //move to appropriate page
      winston.info("================================");
      winston.info("Instance url: " + instance_url);
      await driver.get(instance_url);
    }

  
    var pageCounter = 0;
    do {
      //add the page to cart
      await utility.clickElement(driver, locators.addAllItemsToCart, 5000);
      await driver.sleep(8000);

      //continue paginating
      await utility.clickElement(driver, locators.nextButton, 5000);

      //keep us below 50
       pageCounter++;
      if (pageCounter >= 49) {
        break;
      }
    } while (await utility.verifyElement(driver, locators.nextButton, 10000));

    //proceed to cart
    await driver.sleep(5000);
    winston.info("Proceeding to cart...");
    await driver.get(cart_url);

    //dumps page content as html 
    var pageSource = await driver.getPageSource();
    

    //push to disk
    var fileName = outputPath + 'Jstash_' + extract + '_' + baseids[i] + '_' + (new Date).getTime().toString() + '.html';
   

    fs.writeFile(fileName, pageSource, function(err) {
      if (err) throw err;
    });
    winston.info('File saved succesfully to local disk.');

    //push to google cloud storage
    await uploadToGC(fileName, gcFolder);
    winston.info('File saved succesfully to Google Cloud Storage.');

    


    //clear cart
    await clearCart(driver);
     winston.info("Successful execution for base " + baseids[i]);
     winston.info(
      "Assimilated " + (pageCounter * 100).toString() + " new records."
    );
    winston.info("================================");
  }
}

/* removes all of our cart contents */
async function clearCart(driverVal) {
  var flag = await utility.verifyElement(
    driverVal,
    locators.clearCartButton,
    5000
  );

  if (flag) {
    await utility.clickElement(
      driverVal,
      locators.clearCartButton,
      utility.GlobalTimeout
    );
    await driverVal.sleep(1000);
    await driverVal
      .switchTo()
      .alert()
      .accept();
    await driverVal.sleep(10000);
  }
}

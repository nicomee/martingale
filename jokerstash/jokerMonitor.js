var winston = require("winston");
winston.info("JokerStash Monitor module initialized, Winston logging active...");

var fs = require("fs");
const path = require("path");
const {Builder, By, Key, until } = require("selenium-webdriver");
var chrome = require("selenium-webdriver/chrome");
var webdriver = require("selenium-webdriver");
var extend = require('selenium-extend');

const Firestore = require("@google-cloud/firestore");
const admin = require("firebase-admin");

var locators = require("./jokerLocators.js");
var utility = require("../utility.js");


//initialize firestore
var keyName = require("../config/mindwise.json");
admin.initializeApp({
  credential: admin.credential.cert(keyName)
});
var db = admin.firestore();
var stuff = [];

db
  .collection("profiles")
  .doc("jokerstash")
  .collection("queue")
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      var newIdentity = {
        status: doc.data().status,
        url: doc.data().url,
        username: doc.data().username,
        pwd: doc.data().pwd,
        captcha: doc.data().captcha,
        extract: doc.data().extract,
        baseids: doc.data().baseids
      };
      stuff.push(newIdentity);
      console.log(stuff);
    });
    return Promise.resolve(stuff);
  })
  .then(stuff => {
    // this will wait for every item in stuff to resolve using the joker() function
    return Promise.all(stuff.map(i => jokerMonitor(i)));
  })
  .then(() => {
    console.log("Done mapping through all the stuff!");
  })
  .catch(reason => {
    console.log(reason);
  });

async function jokerMonitor(stuff) {
  //initialize webdriver & loginMap
  //load blockchain-dns extension
  options = new chrome.Options();
  options.addArguments("--load-extension=/extension");
  options.addArguments("--start-maximized");

  //intialize driver
  var driver = new webdriver.Builder()
    .setChromeOptions(options)
    .withCapabilities({
      browserName: "chrome",
      name: "Chrome",
      tz: "America/Los_Angeles",
      build: "Chrome Build",
      idleTimeout: "60"
    })
    .build();
    extend.addExtend(driver);
  var loginMap = new Map();
  loginMap.set("name", "jstash");
  loginMap.set("url", stuff.url);
  loginMap.set("captchaAttempts", stuff.captcha);
  loginMap.set("userNameEdit", locators.usernameEdit);
  loginMap.set("passwordEdit", locators.passwordEdit);
  loginMap.set("captchaImage", locators.captchaImage);
  loginMap.set("captchaEdit", locators.captchaEdit);
  loginMap.set("loginButton", locators.loginButton);
  loginMap.set("confirmationElement", locators.userNameHeader);
  loginMap.set("userNameVal", stuff.username);
  loginMap.set("passwordVal", stuff.pwd);

  await utility.login(driver, loginMap);

  //check news alert
  if (await utility.verifyElement(driver, locators.newsAlert, 5000)) {
    //get news information
    var data = await driver.findElement(webdriver.By.xpath(locators.newsArticle)).getText();
    winston.info(data);

    var ts = new Date().getTime().toString();
    let baseRef =  db
      .collection("landing")
      .doc("jstash")
      .collection("newbase")
      .doc();
    baseRef
      .create({
        newstext: data,
        ts: timestamp
      })
      .then(res => {
        winston.info("Updated posted for jstash at " + ts);
      })
      .catch(err => {
        winston.info("Failed to post update for jstash : " + err.toString());
      });
  } else {
    winston.info("No new bases on JokerStash.");
  }
}

//he needs to run every hour
jokerMonitor();

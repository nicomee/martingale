// http module should be installed:
// npm i http

// Params:
// your anti-captcha.com account key
var anticaptcha = require('anti-captcha');
var service = anticaptcha('http://anti-captcha.com', 'a4ffd8dd7af0510a2db9d6312408983a');

module.exports = {
    solveCaptcha: async function (base64, param){
        return await service.uploadCaptcha(base64, {phrase: true})
        .then(async function(captcha) {
            return await service.getText(captcha).then(async function(captcha) {
                await console.log("Anticaptcha Response: " + captcha.text)
                if(await captcha.text !== undefined){

                    if(param !== ''){
                        if (await (captcha.text.indexOf(param) > -1)) {
                            return captcha.text;
                        }else{
                            await service.reportBad(captcha);
                            return 'false';
                        }
                    }
                    return captcha.text;
                }else{
                    return 'false';
                }
        });
    }).catch(function(error) {
      console.log('Error:', error);
      return 'false';
    });
  }
};